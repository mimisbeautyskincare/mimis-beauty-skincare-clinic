Mimi and Pelin, both sisters and directors, have been servicing hundreds of happy clients in Cremorne, Mosman and Neutral Bay Sydney for over 12 years.
With microdermabrasion, peels, advanced light therapy, facials they enhance the natural beauty of their clients in their Sydney beauty salon.

Address: 11/287 Military Rd, Cremorne, NSW 2090, Australia

Phone: +61 2 9909 1117